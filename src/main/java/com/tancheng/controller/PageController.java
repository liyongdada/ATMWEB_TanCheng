package com.tancheng.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ApacheMoy on 2017/11/4.
 * 此页专门处理页面请求而不是ajax请求
 */
@Controller
@RequestMapping(value = "/user")
public class PageController {
    @RequestMapping(value = "/loginPage",method = RequestMethod.GET)
    public String getLoginPage(){
        return "login";
    }

}
